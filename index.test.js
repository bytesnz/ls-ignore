const test = require('ava');
const lsIgnore = require('./index');

test('By default uses .gitignore', (t) => {
  return lsIgnore().then((ignore) => {
    t.false(ignore.ignores('test.test.js'));
    t.false(ignore.ignores('index.js'));
    t.false(ignore.ignores('test/npm/test.npmignores'));
    t.true(ignore.ignores('test/npm/test.gitignores'));
    t.true(ignore.ignores('test/nonpm/test.another'));
    t.false(ignore.ignores('test.gitignores'));
  });
});

test('Works like NPM when ignorePath set to true', (t) => {
  return lsIgnore(true).then((ignore) => {
    t.true(ignore.ignores('test.test.js'));
    t.false(ignore.ignores('index.js'));
    t.true(ignore.ignores('test/npm/test.npmignores'));
    t.true(ignore.ignores('test/npm/test.gitignores'));
    t.true(ignore.ignores('test/nonpm/test.another'));
    t.false(ignore.ignores('test.gitignores'));
  });
});

test('Uses only ignorePath when given', (t) => {
  return lsIgnore('.gitignore').then((ignore) => {
    t.false(ignore.ignores('test.test.js'));
    t.false(ignore.ignores('index.js'));
    t.false(ignore.ignores('test/npm/test.npmignores'));
    t.false(ignore.ignores('test/npm/test.gitignores'));
    t.false(ignore.ignores('test/nonpm/test.another'));
    t.false(ignore.ignores('test.gitignores'));
  });
});

test('Searches the given root directory for ignore files', (t) => {
  return Promise.all([
    lsIgnore(false, { rootDir: 'test/npm' }).then((ignore) => {
      t.true(ignore.ignores('test.gitignores'));
      t.false(ignore.ignores('test.npmignores'));
    }),
    lsIgnore(true, { rootDir: 'test/npm' }).then((ignore) => {
      t.false(ignore.ignores('test.gitignores'));
      t.true(ignore.ignores('test.npmignores'));
    })
  ]);
});

test('Does not error when no ignore file is found', (t) => {
  return Promise.all([
    lsIgnore(false, { rootDir: 'test' }).then((ignore) => {
      t.true(ignore.ignores('npm/test.gitignores'));
      t.false(ignore.ignores('npm/test.npmignores'));
    }),
    lsIgnore(true, { rootDir: 'test' }).then((ignore) => {
      t.false(ignore.ignores('npm/test.gitignores'));
      t.true(ignore.ignores('npm/test.npmignores'));
    })
  ]);
});

test('Does not search subdirectories for ignore files when noSearchSubdirectories set', (t) => {
  return lsIgnore(false, { noSearchSubdirectories: true }).then((ignore) => {
    t.false(ignore.ignores('test.test.js'));
    t.false(ignore.ignores('index.js'));
    t.false(ignore.ignores('test.npmignores'));
    t.false(ignore.ignores('test/npm/test.gitignores'));
  });
});

