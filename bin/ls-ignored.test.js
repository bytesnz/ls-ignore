const test = require('ava');
const childProcess = require('child_process');
const util = require('util');

const exec = util.promisify(childProcess.exec);

test('By default uses .gitignore', (t) => {
  return exec('bin/ls-ignored').then((ignored) => {
    t.not(null, ignored.stdout.match(/^\s*index\.js$/m));
    t.not(null, ignored.stdout.match(/^\s*\.tickings$/m));
    t.is(null, ignored.stdout.match(/^\s*test\.gitignores$/m));
  });
});

test('Works like NPM when -n flag used', (t) => {
  return exec('bin/ls-ignored -n').then((ignored) => {
    t.not(null, ignored.stdout.match(/^\s*index\.js$/m));
    t.is(null, ignored.stdout.match(/^\s*\.tickings$/m));
    t.is(null, ignored.stdout.match(/^\s*test\/\.gitignore$/m));
  });
});

test('Uses only ignorePath when given', (t) => {
  return exec('bin/ls-ignored -f .gitignore').then((ignored) => {
    t.not(null, ignored.stdout.match(/^\s*index\.js$/m));
    t.not(null, ignored.stdout.match(/^\s*\.tickings$/m));
    t.not(null, ignored.stdout.match(/^\s*test\.gitignores$/m));
    t.is(null, ignored.stdout.match(/^\s*test\.gitignores~$/m));
  });
});

test('Searches the given root directory for ignore files', (t) => {
  return Promise.all([
    exec('bin/ls-ignored -r test/npm').then((ignored) => {
      t.is(null, ignored.stdout.match(/^\s*test\.gitignores$/m));
      t.not(null, ignored.stdout.match(/^\s*test\.npmignores$/m));
      t.not(null, ignored.stdout.match(/^\s*test\.gitignores~$/m));
    }),
    exec('bin/ls-ignored -n -r test/npm').then((ignored) => {
      t.not(null, ignored.stdout.match(/^\s*test\.gitignores$/m));
      t.is(null, ignored.stdout.match(/^\s*test\.npmignores$/m));
    })
  ]);
});

test('Does not error when no ignore file is found', (t) => {
  return Promise.all([
    exec('bin/ls-ignored -r test').then((ignored) => {
      t.is(null, ignored.stdout.match(/^\s*test\.gitignores$/m));
      t.not(null, ignored.stdout.match(/^\s*test\.npmignores$/m));
      t.not(null, ignored.stdout.match(/^\s*test\.gitignores~$/m));
    }),
    exec('bin/ls-ignored -n -r test').then((ignored) => {
      t.not(null, ignored.stdout.match(/^\s*test\.gitignores$/m));
      t.is(null, ignored.stdout.match(/^\s*test\.npmignores$/m));
    })
  ]);
});

test('Does not print or search subdirectories when noRecurse set', (t) => {
  return exec('bin/ls-ignored -d').then((ignored) => {
    t.not(null, ignored.stdout.match(/^\s*index\.js$/m));
    t.not(null, ignored.stdout.match(/^\s*\.tickings$/m));
    t.is(null, ignored.stdout.match(/^\s*test\.gitignores$/m));
  });
});

test('Prints all files when verbose set', (t) => {
  return exec('bin/ls-ignored -v').then((ignored) => {
    t.not(null, ignored.stdout.match(/^\s*\+\s*index\.js$/m));
    t.not(null, ignored.stdout.match(/^\s*\+\s*\.tickings$/m));
    t.not(null, ignored.stdout.match(/^\s*-\s*test\.gitignores$/m));
  });
});

test('Prints full relative paths per line when paths argument given', (t) => {
  return exec('bin/ls-ignored').then((ignored) => {
    t.not(null, ignored.stdout.match(/^index\.js$/m));
    t.not(null, ignored.stdout.match(/^\.tickings$/m));
    t.is(null, ignored.stdout.match(/^test\/npm\/test\.gitignores$/m));
  });
});

test('Prints the given file and exits with 0 if file is not ignored', (t) => {
  return new Promise((resolve, reject) => {
    let child = childProcess.exec('bin/ls-ignored test/npm/test.npmignores', (error, ignored) => {
      t.is('test/npm/test.npmignores\n', ignored);
      resolve();
    });

    child.on('exit', (code) => {
      t.is(0, code);
    });
  });
});

test('Does not print the given file and exits with 1 if file is ignored', (t) => {
  return new Promise((resolve, reject) => {
    let child = childProcess.exec('bin/ls-ignored test/npm/test.gitignores', (error, ignored) => {
      t.is('', ignored);
      resolve();
    });

    child.on('exit', (code) => {
      t.is(1, code);
    });
  });
});

test('Prints the given file and exits with 0 if the file is ignored and ignore flag set', (t) => {
  return new Promise((resolve, reject) => {
    let child = childProcess.exec('bin/ls-ignored -i test/npm/test.npmignores', (error, ignored) => {
      t.is('', ignored);
      resolve();
    });

    child.on('exit', (code) => {
      t.is(0, code);
    });
  });
});

test('Does not print the given file and exits with 1 if file not ignore and ignored flag set', (t) => {
  return new Promise((resolve, reject) => {
    let child = childProcess.exec('bin/ls-ignored -i test/npm/test.gitignores', (error, ignored) => {
      t.is('test/npm/test.gitignores\n', ignored);
      resolve();
    });

    child.on('exit', (code) => {
      t.is(1, code);
    });
  });
});

