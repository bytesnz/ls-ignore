const ignore = require('ignore');

module.exports = function Ignore() {
  var ignores = {};

  var object = {
    add: function(ignoreString, path) {
      if (!path) {
        path = '';
      }

      if (ignores[path]) {
        ignores[path].add(ignoreString);
      } else {
        ignores[path] = ignore().add(ignoreString);
      }

      return object;
    },
    ignores: function (path) {
      if (Object.keys(ignores).findIndex(function (basePath) {
        if (path.startsWith(basePath)) {
          return ignores[basePath].ignores(path);
        }
      }) !== -1) {
        return true;
      } else {
        return false;
      }
    },
    filter: function (paths) {
      return paths.filter(function (path) {
        return !object.ignores(path);
      });
    }
  };

  return object;
};
