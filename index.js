const pkgDir = require('pkg-dir');
const ignore = require('./lib/ignore');
const fs = require('fs');
const path = require('path');
const glob = require('glob');
const process = require('process');

const checkForFile = (filename) => {
  return new Promise((resolve, reject) => {
    fs.access(filename, fs.constants.R_OK, (error) => {
      if (error) {
        reject(error);
        return;
      }

      resolve(filename);
    });
  });
};

/**
 * Create an ignore Object using either, the .gitignore file, the same
 * configuration as NPM would use, or the given file.
 *
 * @param {boolean|string} [ignorePath] Either falsey to use the .gitignore file,
 *   a string path to the ignore to use, or truey to use the same
 *   configuration as NPM would use (.npmignore or .gitignore, plus some
 *   special files)
 * @param {object} [options] Additional options
 * @param {boolean} [options.noSearchSubdirectories] Boolean If truey, don't search
 *   subdirectories for ignore files. Will be ignored when a ignore file is
     specified
 * @param {string} [options.rootDir] Specify the directory to list
 */
module.exports = (ignorePath, options) => {
  let foundNpmignore = false;

  if (!options) {
    options = {};
  }

  return new Promise((resolve, reject) => {
    if (typeof ignorePath !== 'string') {
      let rootPromise;
      if (options.rootDir) {
        rootPromise = Promise.resolve(path.resolve(options.rootDir));
      } else if (!ignorePath) {
        rootPromise = Promise.resolve(path.resolve(process.cwd()));
      } else {
        rootPromise = pkgDir();
      }

      rootPromise.then(rootDir => {
        let promise;

        if (rootDir === null) {
          console.error('Could not find root directory to check for ignore '
            + 'files in searching for ' + (ignorePath ? 'a package.json file'
            : '.git folder'));
          process.exit(1);
          return;
        }

        if (ignorePath) {
          // Check for the existence of a .npmignore file
          promise = checkForFile(path.join(rootDir, '.npmignore')).then((filename) => {
            foundNpmignore = true;
            return filename;
          }, (error) => {
            if (error.code === 'ENOENT') {
              return checkForFile(path.join(rootDir, '.gitignore'));
            }

            return Promise.reject(error);
          });
        } else {
          promise = checkForFile(path.join(rootDir, '.gitignore'));
        }

        return promise.catch((error) => {
          if (error.code === 'ENOENT') {
            return null;
          }

          return Promise.reject(error);
        }).then((filename) => {
          let contents = filename ? fs.readFileSync(filename).toString() : '';
          if (ignorePath === true) {
            contents = '.*.swp\n'
                + '._*\n'
                + '.DS_Store\n'
                + '.git\n'
                + '.hg\n'
                + '.npmrc\n'
                + '.lock-wscript\n'
                + '.svn\n'
                + '.wafpickle-*\n'
                + 'config.gypi\n'
                + 'CVS\n'
                + 'npm-debug.log\n'
                + 'node_modules\n'
                + '.gitignore\n'
                + '.npmignore\n'
                + contents;
          } else if (!ignorePath) {
            contents = '/.git\n' + contents;
          }

          const lg = ignore().add(contents);

          if  (!options.noSearchSubdirectories) {
            glob(path.join(rootDir, `*/**/${ignorePath ? '.{npm,git}ignore' : '.gitignore'}`), {
              silent: true
            }, (error, matches) => {
              let promises = [];

              if (error) {
                reject(error);
                return;
              }

              if (ignorePath) {
                // Filter out .gitignores where we have .npmignores
                matches = matches.filter((file) => {
                  const dir = path.dirname(file);
                  const basename = path.basename(file);

                  if (basename === '.gitignore'
                      && matches.indexOf(path.join(dir, '.npmignore')) !== -1) {
                    return false;
                  }

                  return true;
                });
              }

              matches.forEach((file) => {
                const fileDir = path.relative(rootDir, path.dirname(file));
                //Check if the file is in a folder that is ignored
                if(!lg.ignores(fileDir)) {
                  subContents = fs.readFileSync(file).toString().split(/[\r\n]+/);

                  lg.add(subContents.join('\n'), fileDir);
                }
              });
              resolve(lg);
            });
          } else {
            resolve(lg);
          }
        });
      }).catch((error) => {
        reject(error);
      });
    } else {
      return checkForFile(ignorePath).then((filename) => {
        resolve(ignore().add(fs.readFileSync(filename).toString()));
      }).catch((error) => reject(error));
    }
  });
};
