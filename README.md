# ls-ignore
[![npm](https://bytes.nz/b/ls-ignore/npm)](https://www.npmjs.com/package/ls-ignore)

(Node.js) Command line utility for checking what files are ignored by
a project's `.gitignore` and `.npmignore` files. It uses the
[ignore](https://www.npmjs.com/package/ignore) package for its filtering
capabilities.


# Command Line Interface
```
ls-ignored
(ls minus ignored)

List files that are not ignored by .gitignore-like files. By default, will
list all files, including sub directories, of those file not ignored using
the .gitignore files.

usage: ls-ignored [-r, --root <dir>] [-n, --npmignore]
            [-d, --no-recurse] [-f, --file <file>]
            [-i, --ignored] [-v, --verbose] [files...]

    [files...]            Only check and print the given files. Returns a
                          status code of 0 if all files are not ignored and
                          a status code of 1 if one or more files are ignored


    -r, --root <dir>      Give the root directory to find the ignore file
                          and to list files in. Default will use the current
                          directory. Option will be ignored if the file
                          option is used
    -n, --npmingore       Match files as npm would (using the .npmignore file
                          or the .gitignore file if a .npmignore file doesn't
                          exist)
                          exist). Note if no root directory is given, the
                          ignore files for all of the package will be used,
                          then the contents for the current directory will be
                          listed.
    -d, --no-recurse      Do not look in subdirectories for ignore files.
    -p, --paths           Print the full relative path for each file
    -i, --ignored         Print those files ignored instead of not ignored
    -v, --verbose         Print all files, marking included files with a +
                          and ignored files with a -
    -f, --file <file>     Use <file> as the ignore file instead of .gitignore

    -h, --help            Show this help
```

# Javascript Module
The Javascript module is a simple wrapper around the
[ignore](https://www.npmjs.com/package/ignore) package to load a projects
`.gitignore` or `.npmignore` files by default. Note that lsIgnore function
returns Promise that resolves to the Object that can be called like ignore.

```javascript
/**
 * Create an ignore Object using either, the .gitignore file, the same
 * configuration as NPM would use, or the given file.
 *
 * @param {boolean|string} [ignorePath] Either falsey to use the .gitignore file,
 *   a string path to the ignore to use, or truey to use the same
 *   configuration as NPM would use (.npmignore or .gitignore, plus some
 *   special files)
 * @param {object} [options] Additional options
 * @param {boolean} [options.noSearchSubdirectories] Boolean If truey, don't search
 *   subdirectories for ignore files. Will be ignored when a ignore file is
     specified
 * @param {string} [options.rootDir] Specify the directory to list
 */
function lsIgnore(ignorePath, options)
```

## Example
```javascript
const lsIgnore = require('ls-ignore');

// To use the projects .gitignore file
lsIgnore().then((ignore) => {
  // To check if a file will be ignored
  ignore.ignores('another.file');

  // To filter out the files that will be ignored from a list of files
  ignore.filter(['another.file', 'more.files', 'second.file']);
});

// To use the projects .npmignore file (or .gitignore if the .npmignore file
// doesn't exist, like npm will do)
lsIgnore(true);

// To use a custom ignore file
lsignore('somefile');

```

# Changelog

## [2.0.0] - 2018-01-07
### Added
- Ability to specify the root directory to use instead of using pkg-dir
  when a ignore file is not given
- Ability to print full relative paths of each file with argument for
  command line tool

### Removed
- `recurse` argument - does by default. Use `no-recurse` to not recurse

### Changed
- Fixed handling of ignore files in subdirectories
- Changed second Javscript argument to options Object
- Default to using current working directory (CWD) for default (not in NPM
  mode)
- Only use pkg-dir in NPM mode
- `top-level` argument to `no-recurse` argument
- Add .git folde to be ignored by default

## [1.0.5] - 2018-01-06
### Changed
- Fixed README to show Javacsript Module returns a Promise

## [1.0.4] - 2018-10-22
### Changed
- Updated packages, which fixed issue with ? special character

## [1.0.3] - 2018-01-29
### Changed
- Handle and error when can't find a root directory

## [1.0.2] - 2018-01-29
### Changed
- Fix npm ignore functionality to ignore git and npm ignore files

## [1.0.1] - 2017-09-19
### Changed
- Added bang to bin file, so runs correctly

## [1.0.0] - 2017-09-19
### Changed
- Added searching subdirectories for ignore files

## [0.9.0] - 2017-09-18
Initial Release


[2.0.0]: https://gitlab.com/bytesnz/ls-ignore/compare/v1.0.5...v2.0.0
[1.0.5]: https://gitlab.com/bytesnz/ls-ignore/compare/v1.0.4...v1.0.5
[1.0.4]: https://gitlab.com/bytesnz/ls-ignore/compare/v1.0.3...v1.0.4
[1.0.3]: https://gitlab.com/bytesnz/ls-ignore/compare/v1.0.2...v1.0.3
[1.0.2]: https://gitlab.com/bytesnz/ls-ignore/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/bytesnz/ls-ignore/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/bytesnz/ls-ignore/compare/v0.9.0...v1.0.0
[0.9.0]: https://gitlab.com/bytesnz/ls-ignore/tree/v0.9.0
